export default {
  modules: [
    // Using package name
    '@nuxtjs/axios',


    // Inline definition
    function () {}
  ],
  buildModules: [
    '@nuxtjs/pwa',
  ]
}